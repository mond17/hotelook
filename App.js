import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  Dimensions,
  ImageBackground,
  StatusBar,
  Image,
} from 'react-native';
import {
  createAppContainer,
  createSwitchNavigator,
  SafeAreaView,
} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {
  createDrawerNavigator,
  DrawerNavigatorItems,
} from 'react-navigation-drawer';
// import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import {
  Header,
  Icon,
  Button,
  Divider,
  Overlay,
  Input,
  Avatar,
} from 'react-native-elements';

import * as Services from './services/Services';

import styles from './styles/Stylesheet';
import * as Progress from 'react-native-progress';

//screenss

import ExploreMainScreen from './screens/ExploreMainScreen';
import HotelDetailScreen from './screens/HotelDetailScreen';
import RoomDetailScreen from './screens/RoomDetailScreen';
import AdditionalScreen from './screens/AdditionalScreen';
// import { widthPercentageToDP } from 'react-native-responsive-screen';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class SplashScreen extends React.Component {
  forceScreenChange() {}
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('ExploreMainScreen');
    }, 2010);
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <StatusBar backgroundColor="#009c9e" barStyle="light-content" />

        <View style={{flex: 1, justifyContent: 'center'}}>
          <Image
            style={{
              width: 100,
              height: 150,
              alignSelf: 'center',
              // marginTop: 30,
            }}
            source={require('./assets/logo/hotelook_logo.png')}
            resizeMode="stretch"></Image>
          <Image
            style={{width: wp('80%'), height: 150, alignSelf: 'center'}}
            source={require('./assets/logo/textLOGO.png')}
            resizeMode="stretch"></Image>
          <Progress.Bar
            progress={0.4}
            style={{alignSelf: 'center'}}
            width={200}
            color="#009c9e"
            // indeterminateAnimationDuration
            indeterminate={true}
          />
        </View>
      </View>
    );
  }
}

class GlobalModal extends React.Component {
  render() {
    return (
      <View style={{flex: 1, position: 'absolute'}}>
        <Overlay
          isVisible={this.props.isVisible}
          onBackdropPress={this.props.onBackdropPress}>
          <View style={{flex: 1}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={styles.TextBoldLeft}>Global Modal!</Text>
              <Icon
                type="material"
                name="close"
                size={30}
                onPress={this.props.onClosePress}
              />
            </View>
          </View>
        </Overlay>
      </View>
    );
  }
}

class TempScreen extends React.PureComponent {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
          <Text>Feature coming soon!</Text>
        </View>
      </View>
    );
  }
}

const navOps = ({navigation, screenProps}) => ({
  headerLeft: (
    <TouchableHighlight
      style={{margin: 20}}
      onPress={() => {
        navigation.toggleDrawer();
      }}>
      <Icon type="material" name="account-circle" color="#000" size={30} />
    </TouchableHighlight>
  ),
});

const CustomDrawerContentComponent = props => (
  <View
    style={
      {
        // margin: 50,
        // borderRadius: 100,
        // backgroundColor: 'blue',
      }
    }>
    <Divider style={{backgroundColor: '#000', height: 2}} />
    <DrawerNavigatorItems {...props} />
  </View>
);

//Draw Navigator for Main Screen
// const DrawNavigator = createDrawerNavigator(
//   {
//     ExploreMainScreen: {
//       screen: ExploreMainScreen,
//       navigationOptions: {
//         title: 'Home',
//         // drawerIcon: ({focused}) => <Icon type="material" name="home" />,
//       },
//     },
//     LogInScreen: {
//       screen: LogInScreen,
//       navigationOptions: {
//         header: null,
//         title: 'Log In',
//         // drawerIcon: ({focused, tintColor}) => (
//         //   <Icon type="font-awesome" name="id-card" />
//         // ),
//       },
//     },
//   },
//   {
//     initialRouteName: 'ExploreMainScreen',
//     contentComponent: CustomDrawerContentComponent,
//     drawerLockMode: 'unlocked',
//     drawerPosition: 'right',
//   },
// );

const AppNavigator = createSwitchNavigator(
  {
    splashFlow: {
      screen: SplashScreen,
    },

    onBoardingFlow: {
      screen: createStackNavigator({
        // MainScreen: {
        //   screen: DrawNavigator,
        //   navigationOptions: {
        //     header: null,
        //   },
        // },

        ExploreMainScreen: {
          screen: ExploreMainScreen,
          navigationOptions: {
            header: null,
          },
        },
        HotelDetailScreen: {
          screen: HotelDetailScreen,
          navigationOptions: {
            header: null,
          },
        },

        RoomDetailScreen: {
          screen: RoomDetailScreen,
          navigationOptions: {
            header: null,
          },
        },
        AdditionalScreen: {
          screen: AdditionalScreen,
          navigationOptions: {
            header: null,
          },
        },
      }),
    },
  },
  {
    // initialRouteName: 'mainFlow',
    initialRouteName: 'splashFlow',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {modalVisible: false};
  }

  componentDidMount() {
    // Services.globalModal = this;
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <GlobalModal
          isVisible={this.state.modalVisible}
          onBackdropPress={() => {
            this.setState({modalVisible: false});
          }}
          onClosePress={() => {
            this.setState({modalVisible: false});
          }}
        />
        <AppContainer />
      </View>
    );
  }
}

console.disableYellowBox = true;
