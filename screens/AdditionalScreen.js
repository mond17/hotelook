'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  FlatList,
  TextInput,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {
  Button,
  Input,
  Overlay,
  Header,
  Test,
  Icon,
  Divider,
} from 'react-native-elements';
import styles from '../styles/Stylesheet';
import Swiper from 'react-native-swiper';
import * as OpenAnything from 'react-native-openanything';

// import Icon from 'react-native-vector-icons/FontAwesome';

// import NumberInput from '../components/NumberInput';

// import SearchCard from '../components/SearchCard';
// import ScalableText from 'react-native-text';
import * as Services from '../services/Services';
// import Icon from 'react-native-ionicons';
import ActionButton from 'react-native-action-button';

const amenities = [
  {
    title: 'Airconditioning',
    icon: 'dot-circle',
  },
  {
    title: 'Free internet',
    icon: 'wifi',
  },
  {
    title: 'Free breakfast',
    icon: 'fastfood',
    type: 'material',
  },
  {
    title: 'Non-smoking hotel',
    icon: 'smoking-ban',
  },
  {
    title: 'Laundry service',
    icon: 'washer',
  },
  {
    title: 'Free Parking',
    icon: 'parking',
  },
  {
    title: 'Pool',
    icon: 'swimmer',
  },
  {
    title: 'Conference facilities',
    icon: 'suitcase',
  },
  {
    title: 'Restaurant',
    icon: 'utensils-alt',
  },
  {
    title: 'Meeting rooms',
    icon: 'handshake',
  },
];
const data = [
  {
    imageUrl: require('../assets/hotel_pictures/room1.jpeg'),
    title: 'Room A',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room B',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room C',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room D',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room E',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room F',
  },
];
const data2 = [
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel A',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel B',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel C',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel D',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel E',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel F',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel E',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel F',
  },
];
class AdditionalScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: {},
      newData: {},
      data: amenities,
      address: '',
      isVisible: false,
    };
  }

  componentDidMount() {
    // console.log('HEREEEEEEEEE!');
    // Services.getUsers().then(res => {
    //   this.users = res;
    //   const newData = Services.prepDummyData(this.users, 10);
    //   this.setState({listViewData: newData});
    //   console.log('popopopop ETO NA', this.state.listViewData);
    // });
    const data = this.props.navigation.getParam('data');
    console.log('Additional Screen : ', data);
  }
  render() {
    const newData = this.state.newData;
    const address = this.state.address;

    const width = this.state.width;

    // console.log('wowowow ETO NA', array);
    return (
      <View style={{flex: 1}}>
        {/* <ScrollView> */}
        <StatusBar
          barStyle="light-content"
          backgroundColor="transparent"
          translucent={true}
        />

        <View
          style={{
            // flex: 1,
            height: 80,
            elevation: 10,

            backgroundColor: 'white',
            paddingTop: 20,
            // backgroundColor: 'gray',
            // margin: 20,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
            style={{
              top: 40,
              left: 10,
              position: 'absolute',
              // padding: 20,
              zIndex: 1,
            }}>
            <Icon
              style={{
                position: 'absolute',

                // zIndex: 1000,
              }}
              name="chevron-left"
              type="font-awesome"
              color="gray"
              size={30}
            />
          </TouchableOpacity>
        </View>
        <ScrollView style={{flex: 1}}>
          <View style={{flex: 1, paddingTop: 40, paddingLeft: 20}}>
            <Text
              style={{
                textAlign: 'left',
                color: '#000',
                fontSize: 30,
                fontWeight: 'bold',
                //   lineHeight: 22,
              }}>
              Additional prices
              {/* {newData.title} */}
            </Text>
          </View>
          <View style={{flex: 1, paddingLeft: 20, width: 300, height: 50}}>
            <Text
              style={{
                textAlign: 'left',
                color: '#000',
                fontSize: 15,
              }}>
              Extra people
              {/* {newData.address} */}
            </Text>
          </View>
          <View style={{flex: 1, paddingLeft: 20}}>
            <Text
              style={{
                textAlign: 'left',
                color: '#000',
                fontSize: 15,
                marginStart: 10,
              }}>
              No charge
            </Text>
          </View>
          <View style={{padding: 20}}>
            <Divider></Divider>
          </View>
        </ScrollView>
        <Overlay
          isVisible={this.state.isVisible}
          windowBackgroundColor="rgba(153, 153, 153,.4)"
          // "rgba(153, 153, 153,.7)"
          overlayBackgroundColor="white"
          width="100%"
          height="10%"
          overlayStyle={{
            backgroundColor: 'clear',
            elevation: 0,
            borderWidth: 0,
          }}
          onBackdropPress={() => this.setState({isVisible: false})}>
          <View
            style={{
              flex: 1,
              justifyContent: 'space-evenly',
            }}>
            {/* <View style={{justifyContent: 'center'}}> */}
            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  backgroundColor: 'green',
                  padding: 30,
                  borderRadius: 30,
                }}
                onPress={() => OpenAnything.Call('+639087910352')}>
                <Text style={{textAlign: 'center'}}>Call</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  backgroundColor: 'red',
                  padding: 30,
                  borderRadius: 30,
                }}
                onPress={() => OpenAnything.Text('+639087910352')}>
                <Text style={{textAlign: 'center'}}>Message</Text>
              </TouchableOpacity>
              {/* </View> */}
            </View>

            {/* <ActivityIndicator
              // isVisible={!true}
              // hidesWhenStopped={false}
              animating={true}
              size="large"
              color="#EBC686"
            /> */}
          </View>
        </Overlay>
      </View>
    );
  }
}

export default AdditionalScreen;
