'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  FlatList,
  TextInput,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  // Overlay,
} from 'react-native';
import {
  Button,
  Input,
  Overlay,
  Header,
  Test,
  Icon,
} from 'react-native-elements';
import {
  createDrawerNavigator,
  DrawerNavigatorItems,
} from 'react-navigation-drawer';
import styles from '../styles/Stylesheet';

import * as Services from '../services/Services';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const data = [
  {
    hotel_id: 2020001,
    imageUrl: require('../assets/hotel_pictures/harvesthotelpic.png'),
    title: 'Harvest Hotel',
    address:
      '1179 Pio Del Pilar Street, Cabanatuan City, Nueva Ecija, Philippines, 3100',
    latlng: {
      latitude: 15.492878,
      longitude: 120.972378,
    },
  },
  {
    hotel_id: 2020002,
    imageUrl: require('../assets/hotel_pictures/hotelsogopic.jpg'),
    title: 'Hotel Sogo Cabanatuan',
    address:
      'Maharlika Highway, Barangay San Juan, Hermogenes C. Concepcion, Sr., Cabanatuan City, Nueva Ecija, Philippines, 3100',
    latlng: {
      latitude: 15.470957,
      longitude: 120.955847,
    },
  },
  {
    hotel_id: 2020003,
    imageUrl: require('../assets/hotel_pictures/topstarhotel.png'),
    title: 'Top Star Hotel',
    address:
      'Maharlika Highway, H. Concepcion St. , Hermogenes C. Concepcion, Sr., Cabanatuan City, Nueva Ecija, Philippines, 3100',

    latlng: {
      latitude: 15.484176,
      longitude: 120.967042,
    },
  },
  {
    hotel_id: 2020004,
    imageUrl: require('../assets/hotel_pictures/royalcresthotel.jpg'),
    title: 'Royal Crest Hotel',
    address:
      'Maharlika Highway, Cabanatuan City, Nueva Ecija, Philippines, South East Asia, 3100',

    latlng: {
      latitude: 15.484176,
      longitude: 120.967042,
    },
  },
  {
    hotel_id: 2020005,
    imageUrl: require('../assets/hotel_pictures/fredsapartelle.png'),
    title: 'Freds Apartelle and Business Hotel',
    address:
      '#728 Victoria Subdivision, Bitas, Cabanatuan City, Philippines, South East Asia, Asia, 3100',

    latlng: {
      latitude: 15.502336,
      longitude: 120.978774,
    },
  },
  {
    hotel_id: 2020006,
    imageUrl: require('../assets/hotel_pictures/villesmeralda.jpg'),
    title: 'Villa Esmeralda Bryan`s Resort Hotel and Restaurant ',
    address:
      'Nueva Ecija - Brgy.Marcos Village,Aurora Road, Palayan, Palayan, Nueva Ecija, Philippines, 3132',
    latlng: {
      latitude: 15.502336,
      longitude: 120.978774,
    },
  },
  {
    hotel_id: 2020007,
    imageUrl: require('../assets/hotel_pictures/laparilla.png'),
    title: 'La Parilla hotel',
    address:
      ' Zulueta corner Melencio Street, Cabanatuan City, Nueva Ecija 3100',
    latlng: {
      latitude: 15.486363,
      longitude: 120.95978,
    },
  },
  {
    hotel_id: 2020008,
    imageUrl: require('../assets/hotel_pictures/farmhouse.gif'),
    title: 'Farmhouse Hotel and Cafe ',
    address: 'Maharlika Highway Brgy, San Jose City, 3121 Nueva Ecija',
    latlng: {
      latitude: 15.764307,
      longitude: 120.958067,
    },
  },
];

class ExploreMainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: {},
      data: data,
      filtered: [],
      isVisible: true,
      // data2: data2,
    };
  }

  _onChangeText(field, value) {
    console.log(value);
    const temp = {};
    temp[field] = value;
    this.setState(temp);
  }
  gotoLogin() {
    this.hideMenu();
    // this.props.navigation.navigate('LogInScreen');
  }
  componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      const rawData = Services.getHotelInfo();

      this.setState({
        data: rawData,
      });

      console.log('HEREEEEEEEEE!', rawData);
    });
  }
  filterSearch(text) {
    const newData = data.filter(item => {
      const itemData =
        item.title.toUpperCase() + ' ' + item.address.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      search: text,
      filtered: newData, // after filter we are setting users to new array
    });

    console.log(newData);
  }

  updateSearch = search => {
    this.filterSearch(search);
  };
  render() {
    // const Hoteldata = this.state.data;
    var array = data;
    console.log('wowowiwo', array);

    if (this.state.filtered.length > 0) {
      array = this.state.filtered;
    }

    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#009c9e" barStyle="light-content" />
        <View style={{flex: 1}}>
          <View style={{height: 20}}></View>
          <View
            style={{
              backgroundColor: 'white',
              width: wp('88%'),
              borderRadius: 5,
              elevation: 10,
              alignSelf: 'center',
              flexDirection: 'row',
            }}>
            <Icon
              containerStyle={{padding: 10}}
              // raised
              name="search"
              type="font-awesome"
              color="gray"
              size={25}></Icon>
            <TextInput
              flex={1}
              placeholder="Search "
              underlineColorAndroid="transparent"
              onChangeText={this.updateSearch}
              // onSubmitEditing={this.searchSubmit}
            ></TextInput>
          </View>
          <View style={{height: 20}}></View>
          <ScrollView>
            <View style={{padding: 10}}>
              <View style={{padding: 10}}>
                <Text style={[styles.TextBoldLeftBigger, {fontSize: hp('3%')}]}>
                  {/* What can we help you find, John? */}
                  Here's hotel in Nueva Ecija, Enjoy!
                </Text>
              </View>
              <FlatList
                data={array}
                renderItem={({item: rowData}) => {
                  return (
                    <View padding={10}>
                      <TouchableOpacity
                        style={{
                          backgroundColor: 'white',

                          width: '100%',
                          height: 250,
                          borderRadius: 5,
                          elevation: 10,
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}
                        onPress={() => {
                          this.props.navigation.navigate('HotelDetailScreen', {
                            data: rowData,
                          });
                          console.log(rowData, 'me');
                        }}>
                        <View style={{flex: 4}}>
                          <Image
                            borderTopLeftRadius={5}
                            borderTopRightRadius={5}
                            // source={{uri: rowData.imageUrl}}

                            source={
                              rowData.imageUrl
                                ? rowData.imageUrl
                                : {uri: rowData.imageUrl}
                            }
                            style={{width: '100%', height: '100%'}}
                          />
                        </View>
                        <Text
                          style={{
                            flex: 1,
                            padding: 10,
                            fontSize: hp('2.7%'),
                            fontWeight: 'bold',
                          }}>
                          {rowData.title}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  );
                }}
              />
            </View>
          </ScrollView>

          <View style={{height: 20}}></View>
        </View>
        <Overlay
          // flex={1}
          isVisible={this.state.isVisible}
          windowBackgroundColor="rgba(153, 153, 153,.9)"
          overlayBackgroundColor="white"
          width="auto"
          height="auto"
          overlayStyle={{
            borderRadius: 20,
            marginTop: hp('5%'),
            marginBottom: hp('15%'),
          }}
          // onBackdropPress={() => this.setState({isVisible: false})}
        >
          <View style={{flex: 1}}>
            <Image
              style={{
                width: 80,
                height: 40,
                padding: 10,
                alignSelf: 'flex-start',
                marginBottom: 100,
              }}
              source={require('../assets/logo/textLOGO.png')}
              resizeMode="stretch"></Image>
            <ImageBackground
              style={{width: '100%', height: 200, borderRadius: 10}}
              source={require('../assets/logo/welcomeNE.jpg')}
              resizeMode="stretch">
              <View style={{top: 80, padding: 20}}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: hp('5%'),
                    fontWeight: 'bold',
                    color: 'white',
                  }}>
                  Welcome!
                </Text>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: hp('2%'),
                    color: 'white',

                    // fontWeight: 'bold',
                  }}>
                  To NUEVA ECIJA'S Hotel looking app.
                </Text>
              </View>
            </ImageBackground>
            <View style={{padding: 10}}>
              <Text>Developers:</Text>
              <Text>John Joseph Garo</Text>
              <Text>Krissia Maye Biag</Text>
              <Text>Maria Cristina Rocimo</Text>
            </View>
            <TouchableOpacity
              onPress={() => this.setState({isVisible: false})}
              style={{
                marginTop: 20,
                justifyContent: 'center',
                padding: 10,
                backgroundColor: '#009c9e',
                borderRadius: 30,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: 'white',
                  fontSize: 15,
                  // padding: 10,
                  // fontWeight: 'bold',
                  lineHeight: 22,
                }}>
                See Hotels
              </Text>
            </TouchableOpacity>
          </View>
        </Overlay>
      </View>
    );
  }
}

export default ExploreMainScreen;
