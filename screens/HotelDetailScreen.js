'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  FlatList,
  TextInput,
  ImageBackground,
  Dimensions,
} from 'react-native';
import {
  Button,
  Input,
  Overlay,
  Header,
  Test,
  Icon,
  Divider,
} from 'react-native-elements';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import styles from '../styles/Stylesheet';
import Swiper from 'react-native-swiper';
import StyleSheet from 'react-native';
import getDirections from 'react-native-google-maps-directions';
import {Marker} from 'react-native-maps';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

// import Icon from 'react-native-vector-icons/FontAwesome';

// import NumberInput from '../components/NumberInput';

// import SearchCard from '../components/SearchCard';
// import ScalableText from 'react-native-text';
import * as Services from '../services/Services';
// import Icon from 'react-native-ionicons';
import ActionButton from 'react-native-action-button';
import * as OpenAnything from 'react-native-openanything';

const amenities = [
  {
    title: 'Airconditioning',
    icon: 'dot-circle',
    // type: 'font-awesome',
  },
  {
    title: 'Free internet',
    icon: 'wifi',
    // type: 'font-awesome',
  },
  {
    title: 'Free breakfast',
    icon: 'fastfood',
    // type: 'material',
  },
  {
    title: 'Non-smoking hotel',
    icon: 'smoking-ban',
  },
  {
    title: 'Laundry service',
    icon: 'washer',
  },
  {
    title: 'Free Parking',
    icon: 'parking',
  },
  {
    title: 'Pool',
    icon: 'swimmer',
  },
  {
    title: 'Conference facilities',
    icon: 'suitcase',
  },
  {
    title: 'Restaurant',
    icon: 'utensils-alt',
  },
  {
    title: 'Meeting rooms',
    icon: 'handshake',
  },
];
const hotel = [
  {
    hotel_id: 2020001,
    imageUrl: [
      require('../assets/hh/hotelpic.png'),
      require('../assets/hh/executive.png'),
      require('../assets/hh/standardqueen.jpg'),
      require('../assets/hh/standardtwin.png'),
    ],
  },
  {
    hotel_id: 2020002,
    imageUrl: [
      require('../assets/sg/hotelpic.jpg'),
      require('../assets/sg/executive.jpg'),
      require('../assets/sg/regency.jpg'),
      require('../assets/sg/standardtwin.jpg'),
      require('../assets/sg/etc2.jpg'),
      require('../assets/sg/etc1.jpg'),
    ],
  },
  {
    hotel_id: 2020003,
    imageUrl: [
      require('../assets/hotel_pictures/topstarhotel.png'),
      require('../assets/ts/hotelpic1.jpg'),
      require('../assets/ts/etc1.jpg'),
      require('../assets/ts/etc2.jpg'),
      require('../assets/ts/etc3.jpg'),
      require('../assets/ts/etc4.jpg'),
    ],
  },
  {
    hotel_id: 2020004,
    imageUrl: [
      require('../assets/hotel_pictures/royalcresthotel.jpg'),

      require('../assets/rc/etc1.jpg'),
      require('../assets/rc/etc2.jpg'),
      require('../assets/rc/etc3.jpg'),
      require('../assets/rc/etc4.jpg'),
    ],
  },
  {
    hotel_id: 2020005,

    imageUrl: [
      require('../assets/hotel_pictures/fredsapartelle.png'),

      require('../assets/fa/etc1.png'),
      require('../assets/fa/etc2.png'),
      require('../assets/fa/etc3.png'),
      require('../assets/fa/etc4.png'),
      require('../assets/fa/etc4.png'),
    ],
  },
  {
    hotel_id: 2020006,

    imageUrl: [
      require('../assets/hotel_pictures/villesmeralda.jpg'),

      require('../assets/ve/etc1.jpg'),
      require('../assets/ve/etc2.jpg'),
      require('../assets/ve/etc3.jpg'),
      require('../assets/ve/etc4.jpg'),
      require('../assets/ve/etc5.jpg'),
      require('../assets/ve/etc6.jpg'),

    ],
  },
  ,
  {
    hotel_id: 2020007,

    imageUrl: [
      require('../assets/hotel_pictures/laparilla.png'),

      require('../assets/lp/deluxe.gif'),
      require('../assets/lp/standard2pax.gif'),
      require('../assets/lp/standardtwin.gif'),
  

    ],
  },
  {
    hotel_id: 2020008,

    imageUrl: [
      require('../assets/hotel_pictures/farmhouse.gif'),

      require('../assets/fh/etc2.jpg'),

      require('../assets/fh/familyroom.jpg'),
      require('../assets/fh/singleroom.jpg'),
      require('../assets/fh/etc1.jpg'),

      require('../assets/fh/twinroom.jpg'),

  

    ],
  },
];

const rooms = [
  {
    hotel_id: 2020001,
    imageUrl: [
      // require('../assets/hh/hotelpic.png'),
      {
        img: require('../assets/hh/executive.png'),
        title: 'Executive',
        price: '4,136',
        // contactnumber:,
        // email:,
      },
      {
        img: require('../assets/hh/standardqueen.jpg'),
        title: 'Standard Queen',
        price: '3,048',
      },
      {
        img: require('../assets/hh/standardtwin.png'),
        title: 'Standard Twin',
        price: '3,048',
      },
    ],
  },
  {
    hotel_id: 2020002,
    imageUrl: [
      {
        img: require('../assets/sg/executive.jpg'),
        title: 'Executive',
        price: '2,210',
      },
      {
        img: require('../assets/sg/regency.jpg'),

        title: 'Regency',
        price: '1,590',
      },
      {
        img: require('../assets/sg/standardtwin.jpg'),

        title: 'Standard Twin',
        price: '1,360',
      },
    ],
  },
  {
    hotel_id: 2020003,
    imageUrl: [
      {
        img: require('../assets/ts/suite.jpg'),
        title: 'Suite',
        price: '3,995',
      },
      {
        img: require('../assets/ts/superiorqueen.png'),
        title: 'Superior Queen',
        price: '2,497',
      },
      {
        img: require('../assets/ts/supertwin.jpg'),
        title: 'Superior Twin',
        price: '2,497',
      },
    ],
  },
  {
    hotel_id: 2020004,
    imageUrl: [
      // require('../assets/hotel_pictures/royalcresthotel.jpg'),
      {
        img: require('../assets/rc/deluxesingle.jpg'),
        title: 'Deluxe Single',
        price: '2,200',
      },
      {
        img: require('../assets/rc/standardtwin.jpg'),
        title: 'Standard Twin',
        price: '2,450',
      },
    ],
  },
  {
    hotel_id: 2020005,

    imageUrl: [
      // require('../assets/hotel_pictures/fredsapartelle.png'),
      {
        img: require('../assets/fa/standardtwin.png'),
        title: 'Standard Twin',
        price: '1,611',
      },
      {
        img: require('../assets/fa/family.png'),
        title: 'Family',
        price: '1,765',
      },
      {
        img: require('../assets/fa/suite.png'),
        title: 'Suite',
        price: '2,100',
      },
    ],
  },
  {
    hotel_id: 2020006,
//picture nlang mali 
    imageUrl: [
      // require('../assets/hotel_pictures/fredsapartelle.png'),
      {
        img: require('../assets/ve/etc1.jpg'),
        title: 'Family Suite',
        price: '4,286',
      },
      {
        img: require('../assets/ve/etc2.jpg'),
        title: 'Superior Room',
        price: '4,286',
      },
      {
        img: require('../assets/ve/etc4.jpg'),
        title: 'Family Deluxe Site',
        price: '4,911',
      },
    ],
  },
  {
    hotel_id: 2020007,
//picture nlang mali 
    imageUrl: [
      // require('../assets/hotel_pictures/fredsapartelle.png'),
      {
        img: require('../assets/lp/standardtwin.gif'),
        title: 'Standard Twin',
        price: '1,308',
      },
      {
        img: require('../assets/lp/standard2pax.gif'),
        title: 'Standard 2 pax',
        price: '1,308',
      },
      {
        img: require('../assets/lp/deluxe.gif'),
        title: 'Family Deluxe Site',
        price: '2,530',
      },
    ],
  },
  {
    hotel_id: 2020008,
//picture nlang mali 
    imageUrl: [
      // require('../assets/hotel_pictures/fredsapartelle.png'),
      {
        img: require('../assets/fh/familyroom.jpg'),
        title: 'Family Room',
        price: '2,400',
      },
      {
        img: require('../assets/fh/singleroom.jpg'),
        title: 'Single Room',
        price: '1,308',
      },
      {
        img: require('../assets/fh/twinroom.jpg'),
        title: 'Twin Room ',
        price: '1,500',
      },
    ],
  },
];

class HotelDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: {},
      newData: {},
      data: amenities,
      marker: {
        latlang: {
          latitude: '',
          longitude: '',
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        },
        // title: 'Harvest Hotel',
        // description: 'Here we goooo!',
      },
    };
  }

  processHotelimages(id) {
    console.log('Processing Hotel Id: ', id);
    {
      hotel.map((l, i) => {
        if (l.hotel_id == id) {
          this.setState({
            chosenHotelImages: l.imageUrl,
          });
          console.log('TRUE', l.imageUrl);
        }
        // return alert('no hotel id confirmed');
      });

      rooms.map((l, i) => {
        if (l.hotel_id == id) {
          this.setState({
            roomImages: l.imageUrl,
          });
          console.log('TROTHHHH', l.imageUrl);
        }
        // return alert('no hotel id confirmed');
      });
    }
  }
  componentDidMount() {
    const paramdata = this.props.navigation.getParam('data');
    console.log('HotelDetailScreen Data : ', paramdata);
    console.log('chosen hotel id :', paramdata.hotel_id);
    this.processHotelimages(paramdata.hotel_id);
    const width = Dimensions.get('window').width;
    this.setState({newData: paramdata, width: width});
    this.state.marker.latlang = paramdata.latlng;
  }

  // onRegionChange(region) {
  //   this.setState({region});
  // }
  render() {
    const newData = this.state.newData;
    const address = newData.address;
    const width = this.state.width;
    const HotelImagetoLoad = this.state.chosenHotelImages
      ? this.state.chosenHotelImages
      : [];
    const RoomImagetoLoad = this.state.roomImages ? this.state.roomImages : [];

    // console.log('wowowow ETO NA', array);
    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <StatusBar
            barStyle="light-content"
            backgroundColor="transparent"
            translucent={true}
          />

          <View style={{flex: 1}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('ExploreMainScreen');
              }}
              style={{
                top: 30,
                left: 10,
                position: 'absolute',
                // padding: 20,
                zIndex: 1,
              }}>
              <Icon
                style={{
                  position: 'absolute',

                  // zIndex: 1000,
                }}
                name="chevron-left"
                type="font-awesome"
                color="white"
                size={30}
              />
            </TouchableOpacity>

            <View style={{flex: 1}}>
              <Swiper
                height={240}
                onMomentumScrollEnd={(e, state, context) =>
                  console.log('index:', state.index)
                }
                dot={
                  <View
                    style={{
                      backgroundColor: 'rgba(0,0,0,.2)',
                      width: 5,
                      height: 5,
                      borderRadius: 4,
                      marginLeft: 3,
                      marginRight: 3,
                      marginTop: 3,
                      marginBottom: 3,
                    }}
                  />
                }
                activeDot={
                  <View
                    style={{
                      backgroundColor: '#000',
                      width: 8,
                      height: 8,
                      borderRadius: 4,
                      marginLeft: 3,
                      marginRight: 3,
                      marginTop: 3,
                      marginBottom: 3,
                    }}
                  />
                }
                paginationStyle={{
                  bottom: -23,
                }}
                loop>
                {HotelImagetoLoad.map((l, i) => (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      backgroundColor: 'transparent',
                    }}>
                    <Image
                      resizeMode="stretch"
                      style={{
                        width: this.state.width,
                        flex: 1,
                      }}
                      source={l}
                      // {l.imageUrl ? l.imageUrl : {uri: l.imageUrl}}
                    />
                  </View>
                ))}
              </Swiper>
            </View>
            <View style={{flex: 3}}>
              <View style={{flex: 1, paddingTop: 20, paddingLeft: 20}}>
                <Text
                  style={{
                    textAlign: 'left',
                    color: '#000',
                    fontSize: hp('3%'),
                    fontWeight: 'bold',
                    //   lineHeight: 22,
                  }}>
                  {newData.title}
                </Text>
              </View>
              <View style={{flex: 1, paddingLeft: 20, width: 300, height: 50}}>
                <Text
                  style={{
                    textAlign: 'left',
                    color: '#000',
                    fontSize: hp('2%'),
                  }}>
                  {newData.address}
                </Text>
              </View>

              <View style={{padding: 20}}>
                <Divider></Divider>
              </View>

              <View
                style={{
                  flex: 1,
                  height: 150,
                  width: '90%',
                  borderRadius: 30,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  padding: 20,
                }}>
                <View
                  style={[
                    {
                      // paddingTop: 30,
                      flex: 1,
                    },
                    styles.container,
                  ]}>
                  <MapView
                    // provider={PROVIDER_GOOGLE}
                    style={[
                      styles.map,
                      {borderRadius: 30, justifyContent: 'center'},
                    ]}
                    region={{
                      latitude: 15.4865,
                      longitude: 120.9734,
                      latitudeDelta: 0.515,
                      longitudeDelta: 0.012,
                    }}
                    customMapStyle={{
                      justifyContent: 'center',
                      padding: 20,
                    }}>
                    <Marker
                      coordinate={newData.latlng}
                      title={newData.title}
                      description={newData.address}
                    />
                  </MapView>
                </View>
              </View>
              <TouchableOpacity
                style={{
                  marginStart: 20,
                  marginEnd: 20,
                  marginTop: 20,
                  padding: 10,
                  borderRadius: 30,
                  // alignSelf: 'flex-end',
                  backgroundColor: '#009c9e',
                }}
                onPress={() => OpenAnything.Map(newData.title)}>
                <Text
                  style={[
                    styles.TextBoldCenterBigger,
                    {color: 'white', fontSize: hp('3%')},
                  ]}>
                  Get directions
                </Text>
              </TouchableOpacity>
              <View style={{padding: 20}}>
                <Divider></Divider>
              </View>
              <View style={{paddingLeft: 20}}>
                <Text style={[styles.TextBoldLeftBigger, {fontSize: hp('3%')}]}>
                  Amenities
                </Text>
              </View>
              <View style={{paddingLeft: 10}}>
                <FlatList
                  numColumns={2}
                  height={200}
                  data={this.state.data}
                  renderItem={({item: rowData}) => {
                    return (
                      <View style={{flex: 1}}>
                        <View style={{padding: 10}}>
                          <View style={styles.column}>
                            <View style={styles.row}>
                              <View style={styles.bullet}>
                                <Text>{'\u2022' + ' '}</Text>
                              </View>
                              <View style={styles.bulletText}>
                                <Text>
                                  <Text style={styles.normalText}>
                                    {rowData.title}
                                  </Text>
                                </Text>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  }}></FlatList>
              </View>
              <View style={{paddingLeft: 20}}>
                <Text style={styles.TextBoldLeftBigger}>
                  Available Room Types
                </Text>
              </View>

              <View
                style={{
                  flex: 1,
                }}>
                <FlatList
                  // flexWrap="wrap"
                  contentContainerStyle={{margin: 4}}
                  // horizontal
                  numColumns={2}
                  showsHorizontalScrollIndicator={false}
                  // scrollEnabled={false}
                  data={RoomImagetoLoad}
                  renderItem={({item: rowData}) => {
                    return (
                      <View
                        style={
                          {
                            // paddingTop: 5,
                          }
                        }>
                        <TouchableOpacity
                          style={{
                            flex: 1,
                            flexDirection: 'column',
                            margin: 12,

                            backgroundColor: 'white',
                            width: 150,
                            height: 150,
                            borderRadius: 5,
                            elevation: 10,
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}
                          onPress={() => {
                            this.props.navigation.navigate('RoomDetailScreen', {
                              data: rowData,
                              address: address,
                            });
                            console.log(rowData, 'me');
                          }}>
                          <View style={{flex: 4}}>
                            <Image
                              borderTopLeftRadius={5}
                              borderTopRightRadius={5}
                              // source={{uri: rowData.imageUrl}}

                              source={rowData.img}
                              // {
                              //   rowData.imageUrl
                              //     ? rowData.imageUrl
                              //     : {uri: rowData.imageUrl}
                              // }
                              style={{width: '100%', height: '100%'}}
                            />
                          </View>
                          <Text
                            style={{
                              flex: 1,
                              marginBottom: 10,
                              padding: 10,
                              fontWeight: 'bold',
                            }}>
                            {rowData.title}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    );
                  }}
                  keyExtractor={(item, index) => index}
                />
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={{height: 20}} />
        <View
          style={{
            flex: 1,
            bottom: 0,
            height: 500,
            elevation: 30,
            // backgroundColor: 'yellow',
          }}>
          <Text style={styles.TextBoldLeftBigger}>woowowiwow</Text>
        </View>
      </View>
    );
  }
}

export default HotelDetailScreen;
