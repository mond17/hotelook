'use strict';

import React, { version } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  FlatList,
  TextInput,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {
  Button,
  Input,
  Overlay,
  Header,
  Test,
  Icon,
  Divider,
} from 'react-native-elements';
import styles from '../styles/Stylesheet';
import Swiper from 'react-native-swiper';
import * as OpenAnything from 'react-native-openanything';

// import Icon from 'react-native-vector-icons/FontAwesome';

// import NumberInput from '../components/NumberInput';

// import SearchCard from '../components/SearchCard';
// import ScalableText from 'react-native-text';
import * as Services from '../services/Services';
// import Icon from 'react-native-ionicons';
import ActionButton from 'react-native-action-button';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const amenities = [
  {
    title: 'Airconditioning',
    icon: 'dot-circle',
  },
  {
    title: 'Free internet',
    icon: 'wifi',
  },
  {
    title: 'Free breakfast',
    icon: 'fastfood',
    type: 'material',
  },
  {
    title: 'Non-smoking hotel',
    icon: 'smoking-ban',
  },
  {
    title: 'Laundry service',
    icon: 'washer',
  },
  {
    title: 'Free Parking',
    icon: 'parking',
  },
  {
    title: 'Pool',
    icon: 'swimmer',
  },
  {
    title: 'Conference facilities',
    icon: 'suitcase',
  },
  {
    title: 'Restaurant',
    icon: 'utensils-alt',
  },
  {
    title: 'Meeting rooms',
    icon: 'handshake',
  },
];
const data = [
  {
    imageUrl: require('../assets/hotel_pictures/room1.jpeg'),
    title: 'Room A',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room B',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room C',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room D',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room E',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Room F',
  },
];
const data2 = [
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel A',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel B',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel C',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel D',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel E',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel F',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel E',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Hotel F',
  },
];
class RoomDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: {},
      newData: {},
      data: amenities,
      address: '',
      isVisible: false,
    };
  }

  componentDidMount() {
    // console.log('HEREEEEEEEEE!');
    // Services.getUsers().then(res => {
    //   this.users = res;
    //   const newData = Services.prepDummyData(this.users, 10);
    //   this.setState({listViewData: newData});
    //   console.log('popopopop ETO NA', this.state.listViewData);
    // });
    const data = this.props.navigation.getParam('data');
    const address = this.props.navigation.getParam('address');
    console.log('LAST BUT NOT THE LEAST', data, ' ooooo ', address);
    // const width = Dimensions.get('window').width;
    this.setState({newData: data, address: address});
  }
  render() {
    const newData = this.state.newData;
    const address = this.state.address;

    const width = this.state.width;

    // console.log('wowowow ETO NA', array);
    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <StatusBar
            barStyle="light-content"
            backgroundColor="transparent"
            translucent={true}
          />

          <View style={{flex: 1}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('HotelDetailScreen');
              }}
              style={{
                top: 30,
                left: 10,
                position: 'absolute',
                // padding: 20,
                zIndex: 1,
              }}>
              <Icon
                style={{
                  position: 'absolute',

                  // zIndex: 1000,
                }}
                name="chevron-left"
                type="font-awesome"
                color="white"
                size={30}
              />
            </TouchableOpacity>

            <View style={{flex: 1}}>
              <Swiper
                height={240}
                onMomentumScrollEnd={(e, state, context) =>
                  console.log('index:', state.index)
                }
                dot={
                  <View
                    style={{
                      backgroundColor: 'rgba(0,0,0,.2)',
                      width: 5,
                      height: 5,
                      borderRadius: 4,
                      marginLeft: 3,
                      marginRight: 3,
                      marginTop: 3,
                      marginBottom: 3,
                    }}
                  />
                }
                activeDot={
                  <View
                    style={{
                      backgroundColor: '#000',
                      width: 8,
                      height: 8,
                      borderRadius: 4,
                      marginLeft: 3,
                      marginRight: 3,
                      marginTop: 3,
                      marginBottom: 3,
                    }}
                  />
                }
                paginationStyle={{
                  bottom: -23,
                }}
                loop>
                {/* {data.map((l, i) => ( */}
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    backgroundColor: 'transparent',
                  }}>
                  <Image
                    resizeMode="stretch"
                    style={{
                      width: this.state.width,
                      flex: 1,
                    }}
                    source={newData.img}
                  />
                </View>
                {/* ))} */}
              </Swiper>

              {/* <Image
                //   borderTopLeftRadius={5}
                //   borderTopRightRadius={5}

                source={
                  newData.imageUrl ? newData.imageUrl : {uri: newData.imageUrl}
                }
                // resizeMode="contain"
                style={{width: width, height: 300}}
              /> */}
            </View>
            <View style={{flex: 3}}>
              <View style={{paddingTop: 20, paddingLeft: 20}}>
                <Text
                  style={{
                    textAlign: 'left',
                    color: '#000',
                    fontSize: 30,
                    fontWeight: 'bold',
                    //   lineHeight: 22,
                  }}>
                  {newData.title}
                </Text>
              </View>
              <View style={{paddingLeft: 20, width: 250, height: 50}}>
                <Text
                  style={{
                    textAlign: 'left',
                    color: '#000',
                    fontSize: 15,
                  }}>
                  {address}
                </Text>
              </View>

              <View style={{padding: 20}}>
                <Divider></Divider>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, paddingLeft: 20}}>
                  <Text
                    style={{
                      textAlign: 'left',
                      color: 'gray',
                      fontSize: 20,
                      // fontWeight: 'bold',
                      lineHeight: 22,
                    }}>
                    Additional prices
                  </Text>
                </View>
                <View style={{paddingRight: 20}}>
                  <TouchableOpacity
                    style={{justifyContent: 'center', padding: 5}}
                    onPress={() => {
                      this.props.navigation.navigate('AdditionalScreen');
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#389972',
                        fontSize: 15,
                        // padding: 10,
                        // fontWeight: 'bold',
                        lineHeight: 22,
                      }}>
                      See
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{padding: 20}}>
                <Divider></Divider>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, paddingLeft: 20}}>
                  <Text
                    style={{
                      textAlign: 'left',
                      color: 'gray',
                      fontSize: 20,
                      // fontWeight: 'bold',
                      lineHeight: 22,
                    }}>
                    Contact hotel
                  </Text>
                </View>
                <View style={{paddingRight: 20}}>
                  <TouchableOpacity
                    onPress={() => this.setState({isVisible: true})}
                    style={{justifyContent: 'center', padding: 5}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#389972',
                        fontSize: 15,
                        // padding: 10,
                        // fontWeight: 'bold',
                        lineHeight: 22,
                      }}>
                      Call / Message
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            // flex: 1,
            bottom: 0,
            height: 80,
            elevation: 30,
            backgroundColor: 'white',
            // paddingTop: 20,
            // margin: 20,
          }}>
          <View style={{flex: 1,flexDirection:"row"} }>
         
              <View style={{alignSelf: 'flex-start', padding: 20,marginStart:10}}>
                <Text style={styles.TextBoldCenter}>
                  P {newData.price}/ Night
                </Text>
              </View>
              <View style={{ right:0, padding: 20}}>
                <TouchableOpacity style={{borderRadius:10, backgroundColor:"red",width:wp("50%"),height:hp("4%"), justifyContent:"center",}}
                onPress={() => OpenAnything.Email('johnjosephgaro010198@gmail.com')}>
                
                <Text style={[{color:"white",fontSize:18, textAlign:"center"}]}>
                  Email us
                </Text>
                </TouchableOpacity>
                </View>
          </View>
        </View>
        <Overlay
          isVisible={this.state.isVisible}
          windowBackgroundColor="rgba(153, 153, 153,.4)"
          // "rgba(153, 153, 153,.7)"
          overlayBackgroundColor="white"
          width="100%"
          height="10%"
          overlayStyle={{
            backgroundColor: 'clear',
            elevation: 0,
            borderWidth: 0,
          }}
          onBackdropPress={() => this.setState({isVisible: false})}>
          <View
            style={{
              flex: 1,
              justifyContent: 'space-evenly',
            }}>
            {/* <View style={{justifyContent: 'center'}}> */}
            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  backgroundColor: 'green',
                  padding: 30,
                  borderRadius: 30,
                }}
                onPress={() => OpenAnything.Call('+639655672302')}>
                <Text style={{textAlign: 'center'}}>Call</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  backgroundColor: 'red',
                  padding: 30,
                  borderRadius: 30,
                }}
                onPress={() => OpenAnything.Text('+639655672302')}>
                <Text style={{textAlign: 'center'}}>Message</Text>
              </TouchableOpacity>
              {/* </View> */}
            </View>
          </View>
        </Overlay>
      </View>
    );
  }
}

export default RoomDetailScreen;
