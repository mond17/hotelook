'user strict';

import hotel_info from '../data/hotelinfo.json';
import hotel_images from '../data/hotel_images.json';

//NetInfo
export const internet = {connected: '', connectionType: ''};

export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getHotelInfo() {
  // return new Promise((resolve, reject) => {
  //   var timer = setTimeout(() => {
  //     reject({errors: [{message: 'Request timeout!'}]});
  //   }, 60000);

  return [...hotel_info];
  // clearInterval(timer);
  // resolve(...hotel_info);
  // });
}
export function getHotelImages() {
  return [...hotel_images];
}
